import 'dart:async';
import 'dart:convert';
import 'model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;


void main() => runApp(MyApp());



final URL = 'https://jsonplaceholder.typicode.com/posts';
Future createPost(String url, {Map body}) async {

  final result = await http.post(URL);

    if (result == 200) {
      return Post.fromJson(json.decode(result.body)); 
    }
    throw  Exception("Error");
}

class MyApp extends StatelessWidget {
  final Future post;

  MyApp({Key key, this.post}) : super(key: key);
  
  TextEditingController titleControler = new TextEditingController();
  TextEditingController bodyControler = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "post",
      theme: ThemeData(
        primaryColor: Colors.blue,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: Text(' Post'),
          ),
          body: Center(
            child:  Container(
              margin: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: new Column(
                children: [
                   TextField(
                    controller: titleControler,
                    decoration: InputDecoration(
                         labelText: 'Post Title'),
                  ),
                   TextField(
                    controller: bodyControler,
                    decoration: InputDecoration(
                       labelText: 'Post Body'),
                  ),
                   RaisedButton(
                    onPressed: () async {
                      Post newPost =  Post(
                       title: titleControler.text, body: bodyControler.text);
                      Post p = await createPost(URL,
                          body: newPost.toJson());
                      print(p.title);
                        print(p.body);
                    },
                    child: const Text("Submit"),
                  )
                ],
              ),
            ),
          )),
    );
  }
}

